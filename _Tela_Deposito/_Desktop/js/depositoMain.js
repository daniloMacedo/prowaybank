let boleto = {
    email: document.getElementById('inputEmail'),
    cpf: document.getElementById('inputCPF'),
    phone: document.getElementById('inputPhone'),
    value: document.getElementById('inputValue')
}
//variaveis globais
let btn = document.getElementById('btn-next')
let form = document.getElementById('formulario')
let confirmar = false
let c = 0;
let principal = document.getElementById('principalM')
let ocultM = document.getElementById('ocultM')

//funcao para validar o cpf e deixar no formato de cpf
boleto.cpf.addEventListener('input', valida = () => {
    let teste = boleto.cpf.value
    let cpfvalido = /^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}))$/;

    teste = teste.replace(/\D/g, ""); //Remove tudo o que não é dígito

    if (teste.length == 11) {
        if (cpfvalido.test(teste) == false) {

            teste = teste.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
            teste = teste.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
            //de novo (para o segundo bloco de números)
            teste = teste.replace(/(\d{3})(\d{1,2})$/, "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos

            document.getElementById("inputCPF").value = teste;
            boleto.cpf.value = teste
            confirmar = true

        } else {
            console.log("CPF invalido");
            alert('Nao pode conter letras ou caracteres especiais no campo CPF')
            document.getElementById('inputCPF').value = ''
        }
    }
})
validaCPF = () => {

    if (confirmar) {
        confirmar = false
    } else {
        console.log("CPF invalido");
        alert('CPF invalido ou incompleto!')
        document.getElementById('inputCPF').value = ''
    }
}
//metodo para validar o email
validaEmail = () => {
    let teste = boleto.email.value
    let emailValido = /\S+@\S+\.\S+/;

    if (emailValido.test(teste) == false) {
        alert('O formato do email digitado nao e valido!')
        document.getElementById('inputEmail').value = ''
    } else
        boleto.email.value = teste

}

//metodo para validar o telefone
validaPhone = () => {
    let teste = boleto.phone.value
    if (!isNaN(teste)) {
        if (teste.length == 11) {
            teste = teste.replace(/\D/g, "");             //Remove tudo o que não é dígito
            teste = teste.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
            teste = teste.replace(/(\d)(\d{4})$/, "$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
            document.getElementById('inputPhone').value = teste;
            boleto.phone.value = teste
        }
    } else {
        alert('Por favor digite um numero de telefone valido')
        document.getElementById('inputPhone').value = '';
    }
}

//metodo que garante que todos os campos estao preenchidos para liberar o botao next
form.addEventListener('input', function () {

    if (boleto.value.value != '' && boleto.cpf.value != '' &&
        boleto.phone.value != '' && boleto.email.value != '') {
        btn.disabled = false
    } else
        btn.disabled = true
})

//Funcao que vai executar o que acontece quando o botao Prosseguir for clicado
btn.addEventListener('click', function (e) {
    e.preventDefault();
   
    principal.style.display = 'none'
    ocultM.style.display = 'flex'
    let date = new Date();
    let formatBr = formatDate(date)
    let formatValor = formatValue(boleto.value.value)
    ocultM.getElementsByTagName('h2')[0].innerText = `O boleto para Depósito no valor de R$${formatValor} foi gerado`
    ocultM.getElementsByTagName('p')[0].innerText = `Seu boleto foi gerado com data de vencimento ${formatBr}. Não e possível pagar após a data de vencimento!`

})

//Formatando a data que sera mostrada ao usuario
formatDate = (date) => {
    let dia;
    let month;
    let year = zeroAEsquerda(date.getFullYear());
    let semana = date.getDay();

    switch (semana) {
        case 6:
            dia = zeroAEsquerda(date.getDate() + 5);
            month = zeroAEsquerda(date.getMonth() + 1);
            return `${dia}/${month}/${year}`
        case 5:

            dia = zeroAEsquerda(date.getDate() + 4);
            month = zeroAEsquerda(date.getMonth() + 1);
            return `${dia}/${month}/${year}`
        default:

            dia = zeroAEsquerda(date.getDate() +3);
            month = zeroAEsquerda(date.getMonth() + 1);
            return `${dia}/${month}/${year}`
    }
}

zeroAEsquerda = (num) => {
    return num >= 10 ? num : `0${num}`;
}
//Formatar valor que sera mostrado para o usuario
formatValue = (value) =>{
    value = parseFloat(value).toFixed(2);
    value = value.toString().replace('.',',');

    let [parteInt, parteDec] = value.split(',');
    let x = '';
    c = 0;

    for (let i = parteInt.length - 1; i >= 0; i--) {
        if (++c > 3) {
            x = '.' + x;
            c = 1;
        }
        x = parteInt[i] + x;
    }
    x = x + ',' + parteDec;
    value = x;
    return value;
}
