/*JS relativo a pagina pagamento-pix
que exibe as campos para pagamento de 
acordo com opção selecionada no dropdown*/
function hidepix(e) {
    switch (e.value) {
        case "selecione":
            chavesPix.style.display = "none"
            cpf.style.display = "none"
            email.style.display = "none"
            telefone.style.display = "none"
            chave.style.display = "none"
            qr.style.display = "none"
            break
        case "cpf":
            chavesPix.style.display = "block"
            cpf.style.display = "block"
            email.style.display = "none"
            telefone.style.display = "none"
            chave.style.display = "none"
            qr.style.display = "none"
            break
        case "email":
            chavesPix.style.display = "block"
            cpf.style.display = "none"
            email.style.display = "block"
            telefone.style.display = "none"
            chave.style.display = "none"
            qr.style.display = "none"
            break
        case "telefone":
            chavesPix.style.display = "block"
            cpf.style.display = "none"
            email.style.display = "none"
            telefone.style.display = "block"
            chave.style.display = "none"
            qr.style.display = "none"
            break
        case "chave":
            chavesPix.style.display = "block"
            cpf.style.display = "none"
            email.style.display = "none"
            telefone.style.display = "none"
            chave.style.display = "block"
            qr.style.display = "none"
            break
        case "qr":
            chavesPix.style.display = "block"
            cpf.style.display = "none"
            email.style.display = "none"
            telefone.style.display = "none"
            chave.style.display = "none"
            qr.style.display = "block"
            break
        default:
            return
    }
}
/* JS relativo a pagina de pagamento sem codigo
que exibe as opçoes de pagamento de acordo com
a opção selecionada no dropdown*/
function hidesemcodigo(e) {
    switch (e.value) {
        case "escolha":
            estadual.style.display ="none"
            federal.style.display = "none"
            textoParaPagamentoDeTributosRelativosAPessoaFisicaDoBrasilBrasileira.style.display = "none"
            break
        case "estadual":
            estadual.style.display = "block"
            federal.style.display = "none"
            break
        case "federal":
            federal.style.display = "block"
            estadual.style.display = "none"
            break
        default:
            return
    }
}

let hidetextarea = document.getElementsByClassName("inputradio")

for (let i = 0; i < hidetextarea.length; i++) {
    let element = hidetextarea[i]
        element.addEventListener('click', function (e) {
            textoParaPagamentoDeTributosRelativosAPessoaFisicaDoBrasilBrasileira.style.display = "block"
        })
}