//Solicitando acesso a camera
const video = document.querySelector('#video')
if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
  const constraints = { 
    video: true,
    audio: false
  }  
  navigator.mediaDevices.getUserMedia(constraints).then(stream => video.srcObject = stream);
}

//Bar-code detector
const barcodeDetector = new BarcodeDetector({ formats: ['qr_code'] });//PRECISA DA LIBRARY
//const matrixDetector = new BarcodeDetector({ formats: ['data_matrix'] });
//const barcodeDetector = new BarcodeDetector({ formats: ['codebar'] });
//const barcodeDetector = new BarcodeDetector({ formats: ['code_128'] });

const detectCode = () => {
  // Start detecting codes on to the video element
  barcodeDetector.detect(video).then(codes => {
    // If no codes exit function
    if (codes.length === 0) return;
    
    for (const barcode of codes)  {
      // Log the barcode to the console
      console.log(barcode)
    }
  }).catch(err => {
    // Log an error if one happens
    console.error(err);
  })
}
setInterval(detectCode, 100);